-- Add priority column
ALTER TABLE tasks ADD column priority integer not null default 50;
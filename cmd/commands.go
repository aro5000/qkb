package cmd

import (
	"fmt"

	console "qkb/internal/console"
	db "qkb/internal/sql"
	version "qkb/internal/version"

	"github.com/spf13/cobra"
)

var cmdModify = &cobra.Command{
	Use:     "modify",
	Short:   "modify task",
	Long:    "Modify a task",
	Aliases: []string{"mod"},
	Run: func(cmd *cobra.Command, args []string) {
		db.Setup()
		console.ModifyTask()
	},
}

var cmdForward = &cobra.Command{
	Use:     "forward",
	Short:   "forward task",
	Long:    "Move a task forward",
	Aliases: []string{"fwd"},
	Run: func(cmd *cobra.Command, args []string) {
		db.Setup()
		console.MoveTaskForward()
	},
}

var cmdBackward = &cobra.Command{
	Use:     "backward",
	Short:   "backward task",
	Long:    "Move a task backwards",
	Aliases: []string{"back"},
	Run: func(cmd *cobra.Command, args []string) {
		db.Setup()
		console.MoveTaskBackward()
	},
}

var cmdDelete = &cobra.Command{
	Use:     "delete",
	Short:   "delete task",
	Long:    "Delete a task",
	Aliases: []string{"del"},
	Run: func(cmd *cobra.Command, args []string) {
		db.Setup()
		console.DeleteTask()
	},
}

var cmdAdd = &cobra.Command{
	Use:   "add",
	Short: "add task",
	Long:  "Add a task",
	Run: func(cmd *cobra.Command, args []string) {
		db.Setup()
		console.AddTask()
	},
}

var cmdVersion = &cobra.Command{
	Use:   "version",
	Short: "Print the current version and exit",
	Long:  `Print the current version and exit`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("qkb:  %s\n", version.Version())
	},
}

func init() {
	rootCmd.AddCommand(cmdModify)
	rootCmd.AddCommand(cmdAdd)
	rootCmd.AddCommand(cmdDelete)
	rootCmd.AddCommand(cmdForward)
	rootCmd.AddCommand(cmdBackward)
	rootCmd.AddCommand(cmdVersion)
}

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	view "qkb/internal/console"
	db "qkb/internal/sql"
)

var rootCmd = &cobra.Command{
	Use:   "qkb",
	Short: "Quick Kanban Board",
	Long: `Quick Kanban Board

A CLI kanban board for managing WIP
`,
	Run: func(cmd *cobra.Command, args []string) {
		db.Setup()
		view.NOTIFICATION += fmt.Sprintf("[+] DB opened at: %s\n", db.QKB_DB)
		view.ShowBoardAndCommands()
	},
}

func init() {
}

func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

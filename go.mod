module qkb

go 1.17

require (
	github.com/eiannone/keyboard v0.0.0-20200508000154-caf4b762e807
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/spf13/cobra v1.2.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
)

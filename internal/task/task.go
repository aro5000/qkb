package task

type Task struct {
	Title    string `yaml:"Title"`
	Priority int    `yaml:"Priority,omitempty"`
	Details  string `yaml:"Details,omitempty"`
}

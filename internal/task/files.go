package task

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	db "qkb/internal/sql"

	"gopkg.in/yaml.v2"
)

func (t *Task) edit() {
	var cmd *exec.Cmd
	file, err := ioutil.TempFile("", "qkb-")
	if err != nil {
		log.Fatal(err)
	}
	defer os.Remove(file.Name())

	// write what is currently in the struct
	y, err := yaml.Marshal(t)
	if err != nil {
		log.Fatal(err)
	}
	err = ioutil.WriteFile(file.Name(), y, 0600)
	if err != nil {
		log.Fatal(err)
	}
	appendMessageInFile(file.Name(), "Priority can be a number 1-100. The higher the priority the higher in the column it is shown")

	// test to make sure there is valid yaml, otherwise try again twice
	success := false
	attempts := 1
	for !success {

		cmd = exec.Command("vim", file.Name())

		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		success = true
		cmd.Run()

		// read the file and unmarshal the data
		err = t.readAndParse(file.Name())
		if err != nil {
			if attempts > 1 {
				log.Fatal(err)
			} else {
				appendMessageInFile(file.Name(), fmt.Sprint(err))
				success = false
			}
		}
		attempts++
	}
}

func (t *Task) readAndParse(file string) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(data, t)
	if err != nil {
		return err
	}
	if t.Priority < 1 || t.Priority > 100 {
		return errors.New("priority must be between 0-100")
	}

	return nil
}

func (t *Task) Add() int64 {
	t.Title = "Your Title Here"
	t.Details = `Put details here:
[] thing1
[] thing2
`
	t.Priority = 50

	t.edit()

	// after editing take the values from the task and set them to the task record
	var tr db.TaskRecord
	tr.Title = t.Title
	tr.Details = t.Details
	tr.Priority = t.Priority
	return db.AddTask(tr)
}

func (t *Task) Modify(id int) error {
	tr, err := db.GetTask(id)
	if err != nil {
		return err
	}
	// Set the task information from the task record received from the db
	t.Title = tr.Title
	t.Details = tr.Details
	t.Priority = tr.Priority

	t.edit()

	// after editing, set the task record from the new task values to update the db
	tr.Title = t.Title
	tr.Details = t.Details
	tr.Priority = t.Priority
	tr.Id = id
	db.ModifyTask(tr)
	return nil
}

func appendMessageInFile(tmpfile, message string) {
	content, err := ioutil.ReadFile(tmpfile)
	if err != nil {
		log.Fatal(err)
	}
	issueComment := fmt.Sprintf(`
#
# %s
#
`, message)

	err = ioutil.WriteFile(tmpfile, []byte(string(content)+issueComment), 0600)
	if err != nil {
		log.Fatal(err)
	}
}

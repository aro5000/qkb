package task

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
	"testing"
)

func TestReadAndParse(t *testing.T) {
	file := createDummyFile()
	defer os.Remove(file)

	var task Task
	task.Priority = 50
	err := task.readAndParse(file)
	if err != nil {
		t.Errorf("Could not read or unmarshal the example file")
	}
	if task.Title != "test task" || task.Details != "test task details\n" {
		t.Errorf("task values are not what was expected")
	}
}

func TestAppendMessageInFile(t *testing.T) {
	file := createDummyFile()
	appendMessageInFile(file, "test issue")

	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(string(data), "\n")

	if lines[6] != "# test issue" {
		t.Errorf("Unexpected error value: %s", lines[6])
	}
}

func createDummyFile() string {
	file, err := ioutil.TempFile("", "qkb-test-")
	if err != nil {
		log.Fatal(err)
	}
	contents := `
Title: test task
Details: |
  test task details
`
	err = ioutil.WriteFile(file.Name(), []byte(contents), 0600)
	if err != nil {
		log.Fatal(err)
	}
	return file.Name()
}

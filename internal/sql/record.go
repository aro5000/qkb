package sql

type TaskRecords []TaskRecord

type TaskRecord struct {
	Id       int
	Title    string
	Details  string
	Stage_Id int
	Priority int
}

type Stages []Stage

type Stage struct {
	Id   int
	Name string
}

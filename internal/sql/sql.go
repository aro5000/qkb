package sql

import (
	"database/sql"
	"errors"
	"log"
	"os"
	"path/filepath"

	_ "github.com/mattn/go-sqlite3"
)

var QKB_DB string
var QKB_DB_DIR string

func Setup() {
	if os.Getenv("QKB_DB") == "" {
		homedir, err := os.UserHomeDir()
		if err != nil {
			log.Fatal(err)
		}

		QKB_DB_DIR = filepath.Join(homedir, ".qkb")
		QKB_DB = QKB_DB_DIR + "/qkb.db"

		err = os.MkdirAll(QKB_DB_DIR, 0750)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		QKB_DB = os.Getenv("QKB_DB")
	}

	db, err := sql.Open("sqlite3", QKB_DB+"?_foreign_keys=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	sqlStmt := `
	CREATE TABLE IF NOT EXISTS stages (id integer primary key, name text not null, UNIQUE(name));
	INSERT OR IGNORE INTO stages (name) VALUES ("DO"), ("DOING"), ("DONE");
	CREATE TABLE IF NOT EXISTS tasks (id integer primary key,
		title text not null,
		details text,
		stage_id integer not null default 1, priority integer not null default 50,
		FOREIGN KEY (stage_id)
			REFERENCES stages (id)
	);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return
	}
}

func AddTask(tr TaskRecord) int64 {
	db, err := sql.Open("sqlite3", QKB_DB+"?_foreign_keys=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("insert into tasks(title, details, stage_id, priority) values(?, ?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	res, err := stmt.Exec(tr.Title, tr.Details, 1, tr.Priority)
	if err != nil {
		log.Fatal(err)
	}
	tx.Commit()
	id, _ := res.LastInsertId()
	return id
}

func ModifyTask(tr TaskRecord) {
	db, err := sql.Open("sqlite3", QKB_DB+"?_foreign_keys=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("update tasks set title=?, details=?, priority=? where id=?")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(tr.Title, tr.Details, tr.Priority, tr.Id)
	if err != nil {
		log.Fatal(err)
	}
	tx.Commit()
}

func DeleteTask(id int) error {
	db, err := sql.Open("sqlite3", QKB_DB+"?_foreign_keys=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("delete from tasks where id=?")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	res, err := stmt.Exec(id)
	if err != nil {
		return err
	}
	tx.Commit()
	count, _ := res.RowsAffected()
	if count < int64(1) {
		return errors.New("[!] invalid id: no rows to delete")
	} else {
		return nil
	}
}

func MoveTask(tr TaskRecord, direction int) error {
	db, err := sql.Open("sqlite3", QKB_DB+"?_foreign_keys=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("update tasks set stage_id=? where id=?")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec((tr.Stage_Id + direction), tr.Id)
	tx.Commit()
	if err != nil {
		return (err)
	} else {
		return nil
	}
}

func GetTask(id int) (TaskRecord, error) {
	db, err := sql.Open("sqlite3", QKB_DB+"?_foreign_keys=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	stmt, err := db.Prepare("select id, title, details, stage_id, priority from tasks where id = ?")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	var tr TaskRecord

	err = stmt.QueryRow(id).Scan(&tr.Id, &tr.Title, &tr.Details, &tr.Stage_Id, &tr.Priority)
	if err != nil {
		// return this error so it can be displayed as a notification
		return TaskRecord{}, err
	}

	return tr, nil
}

func GetStages() Stages {
	db, err := sql.Open("sqlite3", QKB_DB+"?_foreign_keys=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rows, err := db.Query("select id, name from stages")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var stages Stages

	for rows.Next() {
		var s Stage
		err = rows.Scan(&s.Id, &s.Name)
		if err != nil {
			log.Fatal(err)
		}
		stages = append(stages, s)
	}

	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return stages
}

func GetTasksFromStage(id int) TaskRecords {
	db, err := sql.Open("sqlite3", QKB_DB+"?_foreign_keys=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rows, err := db.Query("SELECT id, title, stage_id from tasks where stage_id = ? ORDER BY priority DESC", id)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var taskrecords TaskRecords

	for rows.Next() {
		var tr TaskRecord
		err = rows.Scan(&tr.Id, &tr.Title, &tr.Stage_Id)
		if err != nil {
			log.Fatal(err)
		}
		taskrecords = append(taskrecords, tr)
	}
	if err != nil {
		log.Fatal(err)
	}

	return taskrecords
}

func DeleteDone() {
	db, err := sql.Open("sqlite3", QKB_DB+"?_foreign_keys=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rows, err := db.Query("select id from stages order by id desc limit 1")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var id int

	for rows.Next() {
		err = rows.Scan(&id)
		if err != nil {
			log.Fatal(err)
		}
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("delete from tasks where stage_id=?")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(id)
	if err != nil {
		log.Fatal(err)
	}
	tx.Commit()
}

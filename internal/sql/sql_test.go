package sql

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"testing"
)

func TestGetTask(t *testing.T) {
	db := createDummyDb()
	os.Setenv("QKB_DB", db)
	defer os.Remove(db)
	defer os.Unsetenv("QKB_DB")
	Setup()
	AddTask(TaskRecord{Title: "test title", Details: "test details"})

	tr, err := GetTask(1)

	if err != nil {
		t.Error("Error getting task id 1")
	}

	if tr.Title != "test title" {
		t.Errorf("Unexpected title: %s", tr.Title)
	}
	if tr.Details != "test details" {
		t.Errorf("Unexpected details: %s", tr.Details)
	}
	if tr.Stage_Id != 1 {
		t.Errorf("Unexpected stage: %v", tr.Stage_Id)
	}
}

func TestMoveTask(t *testing.T) {
	db := createDummyDb()
	os.Setenv("QKB_DB", db)
	defer os.Remove(db)
	defer os.Unsetenv("QKB_DB")
	Setup()
	AddTask(TaskRecord{Title: "test title", Details: "test details"})

	// move task from the first stage to the second stage
	tr, _ := GetTask(1)
	err := MoveTask(tr, 1)
	if err != nil {
		t.Error("Error moving task forward")
	}

	// move task back to the first stage
	tr, _ = GetTask(1)
	err = MoveTask(tr, -1)
	if err != nil {
		t.Error("Error moving task backwards")
	}

	// moving task back again should cause a foreign key constraint issue
	tr, _ = GetTask(1)
	err = MoveTask(tr, -1)
	if fmt.Sprint(err) != "FOREIGN KEY constraint failed" {
		t.Error("Moving task back again should have caused a foreign key constraint error")
	}
}

func TestDeleteTask(t *testing.T) {
	db := createDummyDb()
	os.Setenv("QKB_DB", db)
	defer os.Remove(db)
	defer os.Unsetenv("QKB_DB")
	Setup()
	AddTask(TaskRecord{Title: "test title", Details: "test details"})

	err := DeleteTask(1)
	if err != nil {
		t.Error("Error deleting task")
	}

	_, err = GetTask(1)
	if err == nil {
		t.Error("Get task was successful, but should have failed after delete")
	}
}

func TestDeleteDone(t *testing.T) {
	db := createDummyDb()
	os.Setenv("QKB_DB", db)
	defer os.Remove(db)
	defer os.Unsetenv("QKB_DB")
	Setup()
	AddTask(TaskRecord{Title: "test title", Details: "test details"})

	tr, _ := GetTask(1)
	err := MoveTask(tr, 2)
	if err != nil {
		t.Error("Issue moving task to the DONE stage")
	}
	DeleteDone()
	tr, err = GetTask(1)

	if err == nil {
		t.Error("Get task was successful, but should have failed after DeleteDone", tr)
	}
}

func TestGetStages(t *testing.T) {
	db := createDummyDb()
	os.Setenv("QKB_DB", db)
	defer os.Remove(db)
	defer os.Unsetenv("QKB_DB")
	Setup()

	expected := Stages{
		Stage{
			Id:   1,
			Name: "DO",
		},
		Stage{
			Id:   2,
			Name: "DOING",
		},
		Stage{
			Id:   3,
			Name: "DONE",
		},
	}

	got := GetStages()

	if !reflect.DeepEqual(got, expected) {
		t.Errorf("Unexpected stages. Got %v, Expected: %v", got, expected)
	}
}

func TestGetTasksFromStage(t *testing.T) {
	db := createDummyDb()
	os.Setenv("QKB_DB", db)
	defer os.Remove(db)
	defer os.Unsetenv("QKB_DB")
	Setup()
	AddTask(TaskRecord{Title: "test title", Details: "test details"})
	tr1, _ := GetTask(1)
	AddTask(TaskRecord{Title: "test title1", Details: "test details1"})
	tr2, _ := GetTask(2)

	_ = MoveTask(tr1, 1)
	_ = MoveTask(tr2, 1)

	expected := TaskRecords{
		TaskRecord{
			Id:       1,
			Title:    "test title",
			Stage_Id: 2,
			Priority: 0,
		},
		TaskRecord{
			Id:       2,
			Title:    "test title1",
			Stage_Id: 2,
			Priority: 0,
		},
	}
	got := GetTasksFromStage(2)

	if !reflect.DeepEqual(got, expected) {
		t.Errorf("Unexpected tasks in result. Got %v, Expected %v", got, expected)
	}
}

func createDummyDb() string {
	file, err := ioutil.TempFile("", "qkb-testdb-")
	if err != nil {
		log.Fatal(err)
	}
	return file.Name()
}

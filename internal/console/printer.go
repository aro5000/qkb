package console

import (
	"fmt"
	"os"
	"text/tabwriter"

	db "qkb/internal/sql"
)

var NOTIFICATION string

func ShowBoardAndCommands() {
	clearScreen()
	printTitle()

	var board = []db.TaskRecords{}

	s := db.GetStages()

	// build board slice for each stage - using a slice to preserve order
	for _, v := range s {
		board = append(board, db.GetTasksFromStage(v.Id))
	}
	w := tabwriter.NewWriter(os.Stdout, 8, 8, 1, ' ', tabwriter.Debug)

	// get the length of the largest stage
	MAX_LENGTH := 0
	for _, v := range board {
		if len(v) > MAX_LENGTH {
			MAX_LENGTH = len(v)
		}
	}

	// set stage columns
	columns := "\n "
	space := "\n "
	for _, v := range s {
		columns += fmt.Sprint(v.Id, ": ", v.Name, "\t")
		space += "---\t"
	}

	fmt.Fprint(w, columns)
	fmt.Fprint(w, space)

	for i := 0; i < MAX_LENGTH; i++ {
		row := "\n "

		for _, stage := range board {
			if i < len(stage) {
				row += fmt.Sprint("[", stage[i].Id, ": ", stage[i].Title, "]\t")
			} else {
				row += "\t"
			}
		}
		fmt.Fprint(w, row)
	}
	fmt.Fprint(w, "\t")
	w.Flush()
	printCommands()
	printNotification()
	Prompt()
}

func printTitle() {
	title := `
	QKB: Quick Kanban Board

	`
	fmt.Println(title)
}

func printCommands() {
	commands := `


Commands:
a: Add a new task
m: Modify a task
f: Move a task forward a stage
b: Move a task back a stage
d: Delete a task
z: Delete all tasks in the final stage
q: Quit
`
	fmt.Println(commands)
}

func printNotification() {
	colorReset := "\033[0m"
	colorRed := "\033[31m"
	if NOTIFICATION != "" {
		fmt.Printf("%s%s%s\n", colorRed, NOTIFICATION, colorReset)
	}
	NOTIFICATION = ""
}

package console

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/eiannone/keyboard"

	db "qkb/internal/sql"
	task "qkb/internal/task"
)

func Prompt() {
	choice, _, err := keyboard.GetSingleKey()
	if err != nil {
		panic(err)
	}

	switch choice {
	case 'a':
		AddTask()
	case 'm':
		ModifyTask()
	case 'f':
		MoveTaskForward()
	case 'b':
		MoveTaskBackward()
	case 'd':
		DeleteTask()
	case 'z':
		DeleteDone()
	case 'q':
		clearScreenAndExit()
	default:
		ShowBoardAndCommands()
	}
}

func AddTask() {
	var t task.Task
	id := t.Add()
	NOTIFICATION += fmt.Sprintf("[+] Added task ID: %v", id)
	ShowBoardAndCommands()
}

func ModifyTask() {
	var t task.Task
	var id int
	fmt.Print("\nEnter ID: ")
	fmt.Scan(&id)
	err := t.Modify(id)
	if err != nil {
		NOTIFICATION += fmt.Sprint(err, "\n")
	}
	ShowBoardAndCommands()
}

func MoveTaskForward() {
	var id int
	fmt.Print("\nEnter ID: ")
	fmt.Scan(&id)
	tr, err := db.GetTask(id)
	if err != nil {
		NOTIFICATION += fmt.Sprint(err, "\n")
	} else {
		err = db.MoveTask(tr, 1)
		if err != nil {
			NOTIFICATION = fmt.Sprintf("[!] Issue moving id: %v\n%s\n", id, err)
		}
	}
	ShowBoardAndCommands()
}

func MoveTaskBackward() {
	var id int
	fmt.Print("\nEnter ID: ")
	fmt.Scan(&id)
	tr, err := db.GetTask(id)
	if err != nil {
		NOTIFICATION += fmt.Sprint(err, "\n")
	} else {
		err = db.MoveTask(tr, -1)
		if err != nil {
			NOTIFICATION = fmt.Sprintf("[!] Issue moving id: %v\n%s\n", id, err)
		}
	}
	ShowBoardAndCommands()
}

func DeleteTask() {
	var id int
	fmt.Print("Enter ID: ")
	fmt.Scan(&id)
	err := db.DeleteTask(id)
	if err != nil {
		NOTIFICATION += fmt.Sprint(err, "\n")
	}
	ShowBoardAndCommands()
}

func DeleteDone() {
	db.DeleteDone()
	ShowBoardAndCommands()
}

func clearScreen() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func clearScreenAndExit() {
	clearScreen()
	os.Exit(0)
}

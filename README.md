# Quick Kanban Board
A kanban board in your shell backed with SQLite

![qkb menu](docs/images/qkb-menu.png)

# Install
* Linux & Mac: Unfortunately due to the go-sqlite3 package and CGO I can't easily cross-compile, but this should work fine if you compile yourself with `go build`.
  * If you run into issues, see the [Mac requirements](https://github.com/mattn/go-sqlite3#mac-osx) for the `go-sqlite` package.
* Windows: Not supported at this time. Please feel free to submit MR's if you'd like to see this work for Windows.

# Use
Once you compile/download the qkb binary, you can just put it anywhere in your `$PATH`.

Running `qkb` for the first time will create a new database in `$HOME/.qkb/qkb.db`. You can override this behavior with an environment variable.
```
export QKB_DB="/my/other/path/new.db"
```

You can use the quick commands (shown with `qkb help`) to do something without going through the main screen. For example `qkb add` will directly put you in the view for adding a new task.

Tasks are modified/added/viewed in a `vim` editor by modifying a YAML file. Other editors may be added in the future.

# Migrations
If you see an error like the following, you might be using an old version of `qkb` and need to apply db migrations:
```
        QKB: Quick Kanban Board


2022/01/18 20:39:33 no such column: priority
```

To apply migrations, run the following:
```
cat dbmigrations/migrations.sql | sqlite3 ~/.qkb/qkb.db
```

# Test
To test all packages:
```
go test -v ./...
```

# Why?
I built this because I frequently have several ways work gets to me (and even multiple Jira boards at times). I wanted a single location to limit WIP where I could also include personal things like "follow up with this person". I might infrequently add features from time to time but for now this is mostly to solve my use case :)